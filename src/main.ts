import * as fs from 'node:fs';
import * as path from 'node:path';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app/modules/app.module';
import helmet from 'helmet';
import { ValidationPipe } from 'app/pipes/validation.pipe';

const packagePath: string = path.join(__dirname, '..', 'package.json');
const packageContent: string = fs.readFileSync(packagePath, 'utf8');
const { name, version } = JSON.parse(packageContent);

async function bootstrap() {
  const port = process.env?.APP_PORT;
  const ssl = process.env.SSL === 'true' ? true : false;
  const address = `http${ssl ? 's' : ''}://localhost:${port}`;
  const app = await NestFactory.create(AppModule, { cors: true });
  app.use(helmet());
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(port, () => {
    console.info(
      `-------------> ${name} v${version} was started on port ${port} listening at ${address} <-------------`,
    );
  });
}
bootstrap();
