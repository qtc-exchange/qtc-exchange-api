export enum ErrorDescriptionEnum {
  //Auth
  PASSWORD_INCORRECT = 'Password incorrect',
  FAILED_VERIFY_TOKEN = 'Failed verify token',
  TOKEN_EMPTY = 'Token empty',
  EMAIL_INCORRECT = 'Email incorrect',

  //Exception
  BAD_REQUEST = 'Bad request',
  NOT_FOUND = 'Not found',
}
