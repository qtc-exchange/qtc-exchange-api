import {
  HttpException,
  Injectable,
  NestMiddleware,
  UnauthorizedException,
} from '@nestjs/common';
import { TokenEmptyException } from 'app/exceptions/token-empty.exception';
import { AuthService } from 'app/providers/auth.service';
import { Request, Response, NextFunction } from 'express';

@Injectable()
export class JwtAuthMiddleware implements NestMiddleware {
  constructor(private readonly authService: AuthService) {}
  async use(
    request: Request,
    response: Response,
    next: NextFunction,
  ): Promise<void> {
    const authHeader = request.headers.authorization;
    if (authHeader && authHeader.startsWith('Bearer ')) {
      const token = authHeader.slice(7);
      try {
        const decodedToken = await this.authService.verifyToken(token);
        request.user = decodedToken;
      } catch (err) {
        const error =
          err instanceof HttpException
            ? new UnauthorizedException(err.getResponse())
            : new UnauthorizedException(err.message);
        throw error;
      }
    } else {
      throw new TokenEmptyException();
    }
    next();
  }
}
