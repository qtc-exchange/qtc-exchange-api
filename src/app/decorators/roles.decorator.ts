import { SetMetadata } from '@nestjs/common';
import { UserRoleEnum } from 'database/enum/user.enum';

export const Roles = (
  ...roles: UserRoleEnum[]
): ReturnType<typeof SetMetadata> => SetMetadata('roles', roles);
