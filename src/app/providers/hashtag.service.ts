import { Injectable } from '@nestjs/common';
import { HashtagNewsRelationModel } from 'app/model/hashtag/hashtag-news-relation.model';
import { HashtagModel } from 'app/model/hashtag/hashtag.model';
import { HashtagEntity } from 'database/entities/hashtag.entity';
import { HashtagRepository } from 'database/repositories/hashtag.repository';

@Injectable()
export class HashtagService {
  constructor(private readonly hashtagRepository: HashtagRepository) {}

  async findOneByTag(tag: string): Promise<HashtagEntity> {
    return await this.hashtagRepository.findOneByTag(tag);
  }

  async findOneHashtagNewsByForeignKey(hashtagId = '', newsId = '') {
    return await this.hashtagRepository.findOneHashtagNewsByForeignKey(
      hashtagId,
      newsId,
    );
  }

  async save(model: HashtagModel) {
    return await this.hashtagRepository.save(model);
  }

  async saveRelation(model: HashtagNewsRelationModel) {
    return await this.hashtagRepository.saveRelation(model);
  }

  async saveRelationMany(model: HashtagNewsRelationModel[]) {
    return await this.hashtagRepository.saveRelationMany(model);
  }

  async deleteRelationMany(model: HashtagNewsRelationModel[]) {
    return await this.hashtagRepository.deleteRelationMany(model);
  }
}
