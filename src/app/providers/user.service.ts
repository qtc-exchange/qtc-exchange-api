import { Injectable } from '@nestjs/common';
import { UserEntity } from 'database/entities/user.entity';
import { UserRepository } from 'database/repositories/user.repository';

@Injectable()
export class UserService {
  constructor(private readonly userRepository: UserRepository) {}

  async findOneById(id: string): Promise<UserEntity> {
    return await this.userRepository.findOneById(id);
  }

  async findOneByEmail(email: string): Promise<UserEntity> {
    return await this.userRepository.findOneByEmail(email);
  }
}
