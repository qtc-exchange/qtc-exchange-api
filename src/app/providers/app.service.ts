import { Injectable } from '@nestjs/common';
import * as fs from 'node:fs';
import { InfoDto } from 'app/dto/app/info.dto';

@Injectable()
export class AppService {
  getInfo(): InfoDto {
    const packagePath = './package.json';
    const packageContent = fs.readFileSync(packagePath, 'utf8');
    const { name, description, version, author } = JSON.parse(packageContent);
    const response = {
      name: name,
      description: description,
      version: version,
      author: author,
    } as InfoDto;
    return response;
  }
}
