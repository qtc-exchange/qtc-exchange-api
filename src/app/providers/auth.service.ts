import { Injectable } from '@nestjs/common';
import { UserService } from './user.service';
import * as crypto from 'node:crypto';
import { UserEntity } from 'database/entities/user.entity';
import { EmailIncorrectException } from 'app/exceptions/email-incorrect.exceptions';
import { PasswordIncorrectException } from 'app/exceptions/password-incorrect.exceptions';
import { TokenDto } from 'app/dto/auth/token.dto';
import { SignInModel } from 'app/model/auth/sign-in.model';
import { plainToInstance } from 'class-transformer';
import { JwtService } from '@nestjs/jwt';
import { FailedVerifyTokeyException } from 'app/exceptions/failed-verify-tokey.exception';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UserService,
    private readonly jwtService: JwtService,
  ) {}

  hashPassword(password: string): string {
    return crypto
      .createHmac('sha256', process.env.PASSWORD_SECRET)
      .update(password)
      .digest('hex');
  }

  async validateUser(email: string, password: string): Promise<UserEntity> {
    const user: UserEntity = await this.userService.findOneByEmail(email);
    if (!user) {
      throw new EmailIncorrectException();
    }
    const hashPassword = this.hashPassword(password);
    if (user.password !== hashPassword) {
      throw new PasswordIncorrectException();
    }
    return user;
  }

  getCookieJwtAccessToken(payload: TokenDto): SignInModel {
    const expirationTime = new Date();
    expirationTime.setHours(23, 59, 59, 0);
    const expiresIn =
      Math.floor(expirationTime.getTime() / 1000) -
      Math.floor(Date.now() / 1000);
    return plainToInstance(SignInModel, {
      token_type: 'Bearer',
      access_token: this.jwtService.sign({ payload }, { expiresIn }),
      role: payload.role,
      name:payload.name
    } as SignInModel);
  }

  verifyToken(token: string): Promise<any> {
    try {
      const decoded = this.jwtService.verify(token);
      return decoded.payload;
    } catch {
      throw new FailedVerifyTokeyException();
    }
  }
}
