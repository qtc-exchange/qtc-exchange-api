import { Injectable } from '@nestjs/common';
import { CreateNewsDto } from 'app/dto/news/create-news.dto';
import { NewsRepository } from 'database/repositories/news.repository';
import { plainToInstance } from 'class-transformer';
import { NewsModel } from 'app/model/news/news.model';
import { omit } from 'lodash';
import { SearchNewsDto } from 'app/dto/news/search-news.dto';
import { NewsPaginationModel } from 'app/model/news/news-pagination.model';
import { UpdateNewsDto } from 'app/dto/news/update-news.dto';

@Injectable()
export class NewsService {
  constructor(private readonly newsRepository: NewsRepository) {}

  async findOneById(id = ''): Promise<NewsModel> {
    return await this.newsRepository.findOneById(id);
  }

  async search(dto: SearchNewsDto): Promise<NewsPaginationModel> {
    return await this.newsRepository.search(dto);
  }

  async create(dto: CreateNewsDto): Promise<NewsModel> {
    const model = plainToInstance(
      NewsModel,
      omit(dto, ['hashtag']) as NewsModel,
    );
    return await this.newsRepository.save(model);
  }

  async update(dto: UpdateNewsDto): Promise<NewsModel> {
    const model = plainToInstance(NewsModel, omit(dto, ['hashtag']));
    return await this.newsRepository.save(model);
  }

  async delete(model: NewsModel): Promise<NewsModel> {
    return await this.newsRepository.delete(model);
  }
}
