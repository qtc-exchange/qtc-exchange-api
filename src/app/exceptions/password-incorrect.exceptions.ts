import { HttpException, HttpStatus } from '@nestjs/common';
import { ErrorDescriptionEnum } from 'app/enums/error.enum';

export class PasswordIncorrectException extends HttpException {
  constructor(message = ErrorDescriptionEnum.PASSWORD_INCORRECT) {
    super(
      {
        statusCode: HttpStatus.UNAUTHORIZED,
        statusText: HttpStatus[HttpStatus.UNAUTHORIZED],
        message,
      },
      HttpStatus.UNAUTHORIZED,
    );
  }
}
