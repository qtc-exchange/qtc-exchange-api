import { HttpException, HttpStatus } from '@nestjs/common';
import { ErrorDescriptionEnum } from 'app/enums/error.enum';

export class EmailIncorrectException extends HttpException {
  constructor(message = ErrorDescriptionEnum.EMAIL_INCORRECT) {
    super(
      {
        statusCode: HttpStatus.UNAUTHORIZED,
        statusText: HttpStatus[HttpStatus.UNAUTHORIZED],
        message,
      },
      HttpStatus.UNAUTHORIZED,
    );
  }
}
