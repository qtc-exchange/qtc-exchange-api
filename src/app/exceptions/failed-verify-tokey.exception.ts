import { HttpException, HttpStatus } from '@nestjs/common';
import { ErrorDescriptionEnum } from 'app/enums/error.enum';

export class FailedVerifyTokeyException extends HttpException {
  constructor(message = ErrorDescriptionEnum.FAILED_VERIFY_TOKEN) {
    super(
      {
        statusCode: HttpStatus.BAD_REQUEST,
        statusText: HttpStatus[HttpStatus.BAD_REQUEST],
        message,
      },
      HttpStatus.UNAUTHORIZED,
    );
  }
}
