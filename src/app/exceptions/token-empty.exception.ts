import { HttpException, HttpStatus } from '@nestjs/common';
import { ErrorDescriptionEnum } from 'app/enums/error.enum';

export class TokenEmptyException extends HttpException {
  constructor(message = ErrorDescriptionEnum.TOKEN_EMPTY) {
    super(
      {
        statusCode: HttpStatus.UNAUTHORIZED,
        statusText: HttpStatus[HttpStatus.UNAUTHORIZED],
        message,
      },
      HttpStatus.UNAUTHORIZED,
    );
  }
}
