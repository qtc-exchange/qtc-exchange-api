import { BadRequestException, NotFoundException } from '@nestjs/common';
import {
  HttpException,
  InternalServerErrorException,
  UnauthorizedException,
} from '@nestjs/common';

export const HandleErrorException = (error: {
  getResponse: () => string | Record<string, any>;
  getStatus: () => number;
  message: any;
}): Error => {
  if (error instanceof UnauthorizedException) {
    return new UnauthorizedException();
  } else if (error instanceof BadRequestException) {
    return new HttpException(error?.getResponse(), error?.getStatus());
  } else if (error instanceof InternalServerErrorException) {
    return new InternalServerErrorException();
  } else if (error instanceof NotFoundException) {
    return new NotFoundException(error.getResponse());
  } else {
    return new InternalServerErrorException();
  }
};
