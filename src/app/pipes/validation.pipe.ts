import {
  PipeTransform,
  Injectable,
  ArgumentMetadata,
  BadRequestException,
  ValidationError,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ValidationPipe implements PipeTransform<ValidationError> {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    const object = plainToClass(metatype, value);
    const errors = await validate(object);
    if (errors.length > 0) {
      throw new BadRequestException(this.formatErrors(errors));
    }
    return value;
  }

  // private toValidate(metatype: Function): boolean {
  //   const types: Function[] = [String, Boolean, Number, Array, Object];
  //   return !types.includes(metatype);
  // }

  private toValidate(metatype: { new (...args: any[]): any }): boolean {
    const types: { new (...args: any[]): any }[] = [
      String,
      Boolean,
      Number,
      Array,
      Object,
    ];
    return !types.some((type) => metatype === type);
  }

  private formatErrors(errors: ValidationError[]) {
    const _errors = errors.map((err) => {
      return {
        name: err.property,
        message: Object.values(err.constraints).join(' and '),
      };
    });
    return _errors;
  }
}
