import {
  HttpException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { UserEntity } from 'database/entities/user.entity';
import { AuthService } from 'app/providers/auth.service';
import { EmailIncorrectException } from 'app/exceptions/email-incorrect.exceptions';
@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly authService: AuthService) {
    super();
  }
  async validate(email: string, password: string): Promise<UserEntity> {
    try {
      const user: UserEntity = await this.authService.validateUser(
        email,
        password,
      );
      if (!user) {
        throw new EmailIncorrectException();
      }
      return user;
    } catch (err) {
      const error =
        err instanceof HttpException
          ? new UnauthorizedException(err.getResponse())
          : new UnauthorizedException(err.message);
      throw error;
    }
  }
}
