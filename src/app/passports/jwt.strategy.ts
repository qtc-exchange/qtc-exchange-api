import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly configService: ConfigService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.get<string>('JWT_SECRET_KEY'),
    });
  }
  validate(payload: any): any {
    const exp = new Date(payload.exp * 1000).toLocaleString('en-US', {
      timeZone: 'asia/bangkok',
    });
    const serverTime = new Date().toLocaleString('en-US', {
      timeZone: 'asia/bangkok',
    });
    return {
      id: payload.payload.id,
      email: payload.payload.email,
      role: payload.payload.role,
      serverTime: serverTime,
      expireIn: exp,
    };
  }
}
