import {
  Controller,
  UsePipes,
  ValidationPipe,
  Post,
  Req,
  Body,
  Query,
  Param,
  Patch,
  NotFoundException,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { CreateNewsDto } from 'app/dto/news/create-news.dto';
import { HandleErrorException } from 'app/exceptions/handle-error.exception';
import { NewsService } from 'app/providers/news.service';
import { Request } from 'express';
import { plainToInstance } from 'class-transformer';
import { UserModel } from 'app/model/user/user.model';
import { HashtagService } from 'app/providers/hashtag.service';
import { HashtagModel } from 'app/model/hashtag/hashtag.model';
import { HashtagNewsRelationModel } from 'app/model/hashtag/hashtag-news-relation.model';
import { NewsResponseModel } from 'app/model/news/news-response.model';
import { Get } from '@nestjs/common';
import { SearchNewsDto } from 'app/dto/news/search-news.dto';
import { MetadataModel } from 'app/model/base/metadata.model';
import { NewsPaginationResponseModel } from 'app/model/news/news-pagination-response.model';
import { UpdateNewsDto } from 'app/dto/news/update-news.dto';
import { isEqual } from 'lodash';
import { Roles } from 'app/decorators/roles.decorator';
import { UserRoleEnum } from 'database/enum/user.enum';
import { RolesGuard } from 'app/guards/roles.guard';

@Controller('news')
@UseGuards(RolesGuard)
export class NewsController {
  constructor(
    private readonly newsService: NewsService,
    private readonly hashtagService: HashtagService,
  ) {}

  @Get()
  async search(@Query() dto: SearchNewsDto) {
    try {
      const newsPagination = await this.newsService.search(dto);
      const metadata: MetadataModel = {
        page: dto.page,
        perPage: dto.limit,
        totalItems: newsPagination.totalItems,
      } as MetadataModel;
      const response = new NewsPaginationResponseModel(
        newsPagination.news,
        metadata,
      );
      return response;
    } catch (err) {
      console.log(err);
      throw HandleErrorException(err);
    }
  }

  @Get('/:id')
  async getOne(@Param('id') id: string) {
    try {
      const news = await this.newsService.findOneById(id);
      if (news) {
        await this.newsService.update(
          plainToInstance(UpdateNewsDto, {
            id: news.id,
            view: news.view + 1,
          } as UpdateNewsDto),
        );
      } else {
        throw new NotFoundException();
      }
      const response = new NewsResponseModel(news);
      return response;
    } catch (err) {
      throw HandleErrorException(err);
    }
  }

  @Roles(UserRoleEnum.ADMIN)
  @Post()
  async create(
    @Req() req: Request,
    @Body() body: CreateNewsDto,
  ): Promise<NewsResponseModel> {
    try {
      const user = plainToInstance(UserModel, req.user as UserModel);
      body.user = user;
      const createdNews = await this.newsService.create(body);
      const response = new NewsResponseModel(createdNews);
      if (body?.hashtag) {
        for (const hashtag of body.hashtag) {
          const hashtagRelationModel = new HashtagNewsRelationModel();
          hashtagRelationModel.news = createdNews;
          const hashtagExist = await this.hashtagService.findOneByTag(hashtag);
          if (!hashtagExist) {
            const hashtagModel: HashtagModel = { tag: hashtag } as HashtagModel;
            const createdHashtag = await this.hashtagService.save(hashtagModel);
            hashtagRelationModel.hashtag = createdHashtag;
          } else {
            hashtagRelationModel.hashtag = hashtagExist;
          }
          const createdHashtagRelation = await this.hashtagService.saveRelation(
            hashtagRelationModel,
          );
          response.putHashtag(createdHashtagRelation.hashtag.tag);
        }
      }
      return response;
    } catch (err) {
      throw HandleErrorException(err);
    }
  }

  @Roles(UserRoleEnum.ADMIN)
  @Patch('/:id')
  async update(
    @Body() body: UpdateNewsDto,
    @Param('id') id: string,
  ): Promise<NewsResponseModel> {
    try {
      body.id = id;
      const newsExist = await this.newsService.findOneById(id);
      if (!newsExist) {
        throw new NotFoundException();
      }
      const updatedNews = await this.newsService.update(body);
      const hashtagExist = newsExist.hashtagNewsRelation.map(
        (relation) => relation.hashtag.tag,
      );
      if (body?.hashtag && !isEqual(hashtagExist, body?.hashtag)) {
        const addTag = body.hashtag.filter(
          (tag) => !hashtagExist.includes(tag),
        );
        const removeTag = hashtagExist.filter(
          (tag) => !body.hashtag.includes(tag),
        );
        const createRelationModels = await Promise.all(
          addTag.map(async (tag) => {
            const relationModel = new HashtagNewsRelationModel();
            relationModel.news = updatedNews;

            const hashtag = await this.hashtagService.findOneByTag(tag);
            relationModel.hashtag =
              hashtag ||
              (await this.hashtagService.save({ tag } as HashtagModel));

            return relationModel;
          }),
        );

        const removeRelationModels = await Promise.all(
          removeTag.map(async (tag) => {
            const hashtag = await this.hashtagService.findOneByTag(tag);
            const relationModel =
              await this.hashtagService.findOneHashtagNewsByForeignKey(
                hashtag.id,
                updatedNews.id,
              );
            return relationModel || null;
          }),
        );
        await this.hashtagService.deleteRelationMany(
          removeRelationModels.filter(Boolean),
        );
        updatedNews.hashtagNewsRelation = [
          ...(await this.hashtagService.saveRelationMany(createRelationModels)),
          ...newsExist.hashtagNewsRelation.filter(
            (relation) => !removeTag.includes(relation.hashtag.tag),
          ),
        ];
      }
      const response = new NewsResponseModel(updatedNews);
      return response;
    } catch (err) {
      throw HandleErrorException(err);
    }
  }

  @Roles(UserRoleEnum.ADMIN)
  @Delete('/:id')
  async delete(@Param('id') id: string) {
    try {
      const newsExist = await this.newsService.findOneById(id);
      if (!newsExist) {
        throw new NotFoundException();
      }
      const deletedNews = await this.newsService.delete(newsExist);
      const response = new NewsResponseModel(deletedNews);
      return response;
    } catch (err) {
      throw HandleErrorException(err);
    }
  }
}
