import {
  Controller,
  Post,
  Req,
  UsePipes,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { TokenDto } from 'app/dto/auth/token.dto';
import { LocalAuthGuard } from 'app/guards/local.guard';
import { Request } from 'express';
import { plainToInstance } from 'class-transformer';
import { UserModel } from 'app/model/user/user.model';
import { AuthService } from 'app/providers/auth.service';
import { SignInModel } from 'app/model/auth/sign-in.model';
import { SignInResponseModel } from 'app/model/auth/sign-in-response.model';
import { Get } from '@nestjs/common';
import { UserService } from 'app/providers/user.service';
import { VerifyTokenResponseModel } from 'app/model/auth/verify-token-response.model';
import { HandleErrorException } from 'app/exceptions/handle-error.exception';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @UseGuards(LocalAuthGuard)
  @Post('sign-in')
  async signIn(@Req() req: Request): Promise<SignInResponseModel> {
    try {
      const user = plainToInstance(UserModel, req.user as UserModel);
      const payload: TokenDto = {
        id: user.id,
        email: user.email,
        role: user.role,
        name: user.name,
      } as TokenDto;
      const token: SignInModel =
        this.authService.getCookieJwtAccessToken(payload);
      const response = new SignInResponseModel(token);
      return response;
    } catch (err) {
      throw HandleErrorException(err);
    }
  }

  @Get('verify-token')
  async verifyToken(@Req() req: Request) {
    try {
      const token: TokenDto = plainToInstance(TokenDto, req.user as TokenDto);
      const user = await this.userService.findOneById(token.id);
      const response = new VerifyTokenResponseModel(user);
      return response;
    } catch (err) {
      throw HandleErrorException(err);
    }
  }
}
