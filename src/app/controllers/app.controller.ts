import { Controller, Get } from '@nestjs/common';
import { AppService } from '../providers/app.service';
import { InfoDto } from 'app/dto/app/info.dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getInfo(): InfoDto {
    return this.appService.getInfo();
  }
}
