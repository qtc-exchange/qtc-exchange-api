import { UserRoleEnum } from 'database/enum/user.enum';

export class TokenDto {
  id: string;
  email: string;
  role: UserRoleEnum;
  name: string;
}
