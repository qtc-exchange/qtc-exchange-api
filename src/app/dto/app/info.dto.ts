export interface InfoDto {
  name: string;
  description: string;
  version: string;
  author: string;
}
