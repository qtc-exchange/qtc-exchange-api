import { NewsTypeEnum } from 'database/enum/news.enum';
import { BasePaginationDto } from '../base/base-pagination.dto';
import { IsOptional, IsString, IsEnum } from 'class-validator';

export class SearchNewsDto extends BasePaginationDto {
  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsEnum(NewsTypeEnum)
  type: NewsTypeEnum;

  @IsOptional()
  @IsString()
  hashtag: string;
}
