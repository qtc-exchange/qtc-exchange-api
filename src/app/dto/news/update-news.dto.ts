import { IsNumber } from 'class-validator';
import { IsOptional, IsString, IsArray } from 'class-validator';
export class UpdateNewsDto {
  @IsOptional()
  @IsString()
  title: string;

  @IsOptional()
  @IsString()
  description: string;

  @IsOptional()
  @IsNumber()
  view: number;

  @IsOptional()
  @IsArray()
  hashtag: string[];

  id: string;
}
