import { UserModel } from 'app/model/user/user.model';
import {
  IsNotEmpty,
  IsString,
  IsOptional,
  IsArray,
  IsEnum,
} from 'class-validator';
import { NewsTypeEnum } from 'database/enum/news.enum';
export class CreateNewsDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsNotEmpty()
  @IsString()
  description: string;

  @IsNotEmpty()
  @IsEnum(NewsTypeEnum)
  type: NewsTypeEnum;

  @IsOptional()
  @IsArray()
  hashtag: string[];

  user: UserModel;
}
