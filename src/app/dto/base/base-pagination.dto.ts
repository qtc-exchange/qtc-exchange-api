import { IsNotEmpty, IsNumber } from 'class-validator';
import { Transform } from 'class-transformer';
export class BasePaginationDto {
  @IsNotEmpty()
  @IsNumber()
  @Transform(({ value }) => parseInt(value))
  page: number;

  @IsNotEmpty()
  @IsNumber()
  @Transform(({ value }) => parseInt(value))
  limit: number;
}
