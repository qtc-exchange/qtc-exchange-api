import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NewsController } from 'app/controllers/news.controller';
import { NewsService } from 'app/providers/news.service';
import { NewsEntity } from 'database/entities/news.entity';
import { NewsRepository } from 'database/repositories/news.repository';
import { HashtagModule } from './hashtag.module';

@Module({
  imports: [TypeOrmModule.forFeature([NewsEntity]), HashtagModule],
  controllers: [NewsController],
  providers: [NewsService, NewsRepository],
})
export class NewsModule {}
