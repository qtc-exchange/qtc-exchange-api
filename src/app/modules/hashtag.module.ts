import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { HashtagService } from 'app/providers/hashtag.service';
import { HashtagNewsRelationEntity } from 'database/entities/hashtag-news-relation.entity';
import { HashtagEntity } from 'database/entities/hashtag.entity';
import { HashtagRepository } from 'database/repositories/hashtag.repository';

@Module({
  imports: [
    TypeOrmModule.forFeature([HashtagEntity, HashtagNewsRelationEntity]),
  ],
  providers: [HashtagService, HashtagRepository],
  exports: [HashtagService],
})
export class HashtagModule {}
