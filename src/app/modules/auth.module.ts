import { Module } from '@nestjs/common';
import { AuthController } from 'app/controllers/auth.controller';
import { AuthService } from 'app/providers/auth.service';
import { UserModule } from './user.module';
import { LocalStrategy } from 'app/passports/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { jwtConfig } from 'config/jwt.config';
import { JwtStrategy } from 'app/passports/jwt.strategy';

@Module({
  imports: [JwtModule.registerAsync(jwtConfig), UserModule],
  controllers: [AuthController],
  providers: [AuthService, JwtStrategy, LocalStrategy],
  exports: [AuthService],
})
export class AuthModule {}
