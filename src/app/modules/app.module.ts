import {
  Module,
  NestModule,
  MiddlewareConsumer,
  RequestMethod,
} from '@nestjs/common';
import { AppController } from '../controllers/app.controller';
import { AppService } from '../providers/app.service';
import { DatabaseModule } from './database.module';
import { UserModule } from './user.module';
import { AuthModule } from './auth.module';
import { ValidationPipe } from 'app/pipes/validation.pipe';
import { APP_PIPE } from '@nestjs/core';
import { ConfigurationModule } from './configuration.module';
import { JwtAuthMiddleware } from 'app/middlewares/jwt.middleware';
import { HashtagModule } from './hashtag.module';
import { NewsModule } from './news.module';

@Module({
  imports: [
    DatabaseModule,
    ConfigurationModule,
    UserModule,
    AuthModule,
    HashtagModule,
    NewsModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    {
      provide: APP_PIPE,
      useClass: ValidationPipe,
    },
  ],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): void {
    consumer
      .apply(JwtAuthMiddleware)
      .exclude(
        { path: '/auth/sign-in', method: RequestMethod.POST },
        { path: '/news', method: RequestMethod.GET },
        { path: '/news/:id', method: RequestMethod.GET },
      )
      .forRoutes(
        { path: '*', method: RequestMethod.GET },
        { path: '*', method: RequestMethod.POST },
        { path: '*', method: RequestMethod.PATCH },
        { path: '/auth/verify-token', method: RequestMethod.GET },
      );
  }
}
