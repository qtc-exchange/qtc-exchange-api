import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserController } from 'app/controllers/user.controller';
import { UserService } from 'app/providers/user.service';
import { UserEntity } from 'database/entities/user.entity';
import { UserRepository } from 'database/repositories/user.repository';

@Module({
  imports: [TypeOrmModule.forFeature([UserEntity])],
  controllers: [UserController],
  providers: [UserService, UserRepository],
  exports: [UserService],
})
export class UserModule {}
