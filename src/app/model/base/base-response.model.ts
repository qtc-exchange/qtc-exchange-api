import { HttpStatus } from '@nestjs/common';
export class BaseResponseModel {
  statusCode: number = HttpStatus.OK;
  statusText: string = HttpStatus[HttpStatus.OK];
}
