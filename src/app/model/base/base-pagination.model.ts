import { HttpStatus } from '@nestjs/common';
import { MetadataModel } from './metadata.model';
export class BasePaginationModel {
  statusCode: number = HttpStatus.OK;
  statusText: string = HttpStatus[HttpStatus.OK];
  metadata: MetadataModel;
}
