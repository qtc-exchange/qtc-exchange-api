export interface MetadataModel {
  page: number;
  perPage: number;
  totalItems: number;
}
