import { NewsEntity } from 'database/entities/news.entity';

export class NewsModel extends NewsEntity {
  hashtag?: string[];
}
