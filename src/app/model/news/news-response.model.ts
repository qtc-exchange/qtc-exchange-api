import { omit } from 'lodash';
import { BaseResponseModel } from '../base/base-response.model';
import { NewsModel } from './news.model';
import { plainToInstance } from 'class-transformer';

export class NewsResponseModel extends BaseResponseModel {
  constructor(model: NewsModel) {
    super();
    this.data = plainToInstance(NewsModel, {
      ...omit(model, ['hashtagNewsRelation', 'deletedAt']),
      hashtag:
        model?.hashtagNewsRelation?.map((relation) => relation.hashtag.tag) ||
        [],
    });
  }
  data: NewsModel;

  putHashtag(tag: string) {
    this.data.hashtag = [...this.data.hashtag, tag];
  }
}
