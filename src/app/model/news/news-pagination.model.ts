import { NewsModel } from './news.model';

export class NewsPaginationModel {
  news: NewsModel[];
  totalItems: number;
}
