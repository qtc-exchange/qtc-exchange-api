import { BasePaginationModel } from '../base/base-pagination.model';
import { MetadataModel } from '../base/metadata.model';
import { NewsModel } from './news.model';
import { plainToInstance } from 'class-transformer';
import { omit } from 'lodash';

export class NewsPaginationResponseModel extends BasePaginationModel {
  constructor(model: NewsModel[], metadata: MetadataModel) {
    super();
    this.metadata = metadata;
    this.data = model.map((news) =>
      plainToInstance(NewsModel, {
        ...omit(news, ['hashtagNewsRelation', 'user', 'deletedAt']),
        hashtag:
          news?.hashtagNewsRelation?.map((relation) => relation.hashtag.tag) ||
          [],
      } as NewsModel),
    );
  }
  data: NewsModel[];
}
