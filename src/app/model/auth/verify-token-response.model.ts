import { BaseResponseModel } from '../base/base-response.model';
import { UserModel } from '../user/user.model';
import { omit } from 'lodash';
import { plainToInstance } from 'class-transformer';

export class VerifyTokenResponseModel extends BaseResponseModel {
  constructor(model: UserModel) {
    super();
    this.user = plainToInstance(
      UserModel,
      omit(model, ['password', 'deletedAt']),
    );
  }
  user: UserModel;
}
