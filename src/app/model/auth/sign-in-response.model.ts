import { BaseResponseModel } from '../base/base-response.model';
import { SignInModel } from './sign-in.model';

export class SignInResponseModel extends BaseResponseModel {
  constructor(model: SignInModel) {
    super();
    this.data = model;
  }
  data: SignInModel;
}
