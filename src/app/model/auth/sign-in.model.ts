import { UserRoleEnum } from 'database/enum/user.enum';

export class SignInModel {
  token_type: string;
  access_token: string;
  role: UserRoleEnum;
  name: string;
}
