import { dataSourceOptions } from './database.config';

module.exports = {
  ...dataSourceOptions,
  requestTimeout: 3_000_000,
  pool: {
    max: 1000,
    min: 1,
    idleTimeoutMillis: 120_000_000,
    acquireTimeoutMillis: 120_000_000,
  },
  cli: {
    migrationsDir: 'dist/database/migrations',
  },
  seeds: ['dist/database/seeds/*.seed{.ts,.js}'],
};
