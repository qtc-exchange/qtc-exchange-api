import { config as dotenvConfig } from 'dotenv';
import { DataSource, DataSourceOptions } from 'typeorm';

dotenvConfig({ path: '.env' });
export const dataSourceOptions: DataSourceOptions = {
  type: 'mysql',
  host: process.env.DATABASE_HOST,
  port: Number.parseInt(process.env.DATABASE_PORT, 10),
  username: process.env.DATABASE_USER,
  password: process.env.DATABASE_PASS,
  database: process.env.DATABASE_NAME,
  entities: ['dist/database/entities/*.entity{.ts,.js}'],
  migrations: [`dist/database/migrations/*.js`],
  charset: 'utf8',
  synchronize: false,
  extra: {
    charset: 'utf8',
  },
};

const dataSource = new DataSource({ ...dataSourceOptions });
export default dataSource;
