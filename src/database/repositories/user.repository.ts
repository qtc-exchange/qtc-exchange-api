import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from 'database/entities/user.entity';
import { Repository } from 'typeorm';

@Injectable()
export class UserRepository {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}

  async findOneById(id = ''): Promise<UserEntity> {
    return await this.userRepository.findOneBy({ id: id });
  }

  async findOneByEmail(email = ''): Promise<UserEntity> {
    return await this.userRepository.findOneBy({ email: email });
  }
}
