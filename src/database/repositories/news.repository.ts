import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { SearchNewsDto } from 'app/dto/news/search-news.dto';
import { NewsPaginationModel } from 'app/model/news/news-pagination.model';
import { NewsEntity } from 'database/entities/news.entity';
import { isNil } from 'lodash';
import { Repository } from 'typeorm';
import { plainToInstance } from 'class-transformer';
import { NewsModel } from 'app/model/news/news.model';

@Injectable()
export class NewsRepository {
  constructor(
    @InjectRepository(NewsEntity)
    private readonly newsRepository: Repository<NewsEntity>,
  ) {}

  async findOneById(id = ''): Promise<NewsModel> {
    return await this.newsRepository.findOne({
      where: { id: id },
      relations: { hashtagNewsRelation: { hashtag: true }, user: true },
    });
  }

  async search(dto: SearchNewsDto) {
    try {
      const query = this.newsRepository
        .createQueryBuilder('news')
        .select('news')
        .addSelect('hashtagNewsRelation')
        .addSelect('hashtag')
        .leftJoin('news.user', 'user')
        .leftJoin('news.hashtagNewsRelation', 'hashtagNewsRelation')
        .leftJoin('hashtagNewsRelation.hashtag', 'hashtag')
        .orderBy('news.createdAt', 'DESC');
      if (!isNil(dto.title)) {
        query.andWhere('news.title like :news_title', {
          news_title: `%${dto.title}%`,
        });
      }
      if (!isNil(dto.type)) {
        query.andWhere('news.type = :news_type', {
          news_type: dto.type,
        });
      }
      if (!isNil(dto.hashtag)) {
        query.andWhere('hashtag.tag = :hashtag_tag', {
          hashtag_tag: dto.hashtag,
        });
      }
      query.skip((dto.page - 1) * dto.limit).take(dto.limit);
      const result = await query.getManyAndCount();
      const [news, totalItems] = result;
      return plainToInstance(NewsPaginationModel, {
        news: news,
        totalItems,
      });
    } catch (err) {
      throw new InternalServerErrorException();
    }
  }

  async save(model: NewsEntity): Promise<NewsEntity> {
    try {
      const entity: NewsEntity = this.newsRepository.create(model);
      const saved: NewsEntity = await this.newsRepository.save(entity);
      return saved;
    } catch (err) {
      throw new InternalServerErrorException();
    }
  }

  async delete(model: NewsEntity): Promise<NewsEntity> {
    try {
      const entity: NewsEntity = this.newsRepository.create(model);
      const deleted: NewsEntity = await this.newsRepository.remove(entity);
      return deleted;
    } catch (err) {
      throw new InternalServerErrorException();
    }
  }
}
