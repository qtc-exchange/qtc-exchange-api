import { Injectable, InternalServerErrorException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { HashtagNewsRelationEntity } from 'database/entities/hashtag-news-relation.entity';
import { HashtagEntity } from 'database/entities/hashtag.entity';
import { Repository } from 'typeorm';

@Injectable()
export class HashtagRepository {
  constructor(
    @InjectRepository(HashtagEntity)
    private readonly hashtagRepository: Repository<HashtagEntity>,
    @InjectRepository(HashtagNewsRelationEntity)
    private readonly hashtagNewsRepository: Repository<HashtagNewsRelationEntity>,
  ) {}

  async findOneHashtagNewsByForeignKey(
    hashtagId = '',
    newsId = '',
  ): Promise<HashtagNewsRelationEntity> {
    return await this.hashtagNewsRepository.findOne({
      where: {
        hashtag: { id: hashtagId },
        news: { id: newsId },
      },
      relations: { hashtag: true, news: true },
    });
  }

  async findOneByTag(tag = ''): Promise<HashtagEntity> {
    return await this.hashtagRepository.findOneBy({ tag: tag });
  }

  async save(model: HashtagEntity): Promise<HashtagEntity> {
    try {
      const entity: HashtagEntity = this.hashtagRepository.create(model);
      const saved: HashtagEntity = await this.hashtagRepository.save(entity);
      return saved;
    } catch (err) {
      throw new InternalServerErrorException();
    }
  }

  async saveRelation(
    model: HashtagNewsRelationEntity,
  ): Promise<HashtagNewsRelationEntity> {
    try {
      const entity: HashtagNewsRelationEntity =
        this.hashtagNewsRepository.create(model);
      const saved: HashtagNewsRelationEntity =
        await this.hashtagNewsRepository.save(entity);
      return saved;
    } catch (err) {
      throw new InternalServerErrorException();
    }
  }

  async saveRelationMany(
    models: HashtagNewsRelationEntity[],
  ): Promise<HashtagNewsRelationEntity[]> {
    try {
      const entity: HashtagNewsRelationEntity[] =
        this.hashtagNewsRepository.create(models);
      const saved: HashtagNewsRelationEntity[] =
        await this.hashtagNewsRepository.save(entity);
      return saved;
    } catch (err) {
      throw new InternalServerErrorException();
    }
  }

  async deleteRelationMany(
    models: HashtagNewsRelationEntity[],
  ): Promise<HashtagNewsRelationEntity[]> {
    try {
      const entity: HashtagNewsRelationEntity[] =
        this.hashtagNewsRepository.create(models);
      const deleted: HashtagNewsRelationEntity[] =
        await this.hashtagNewsRepository.remove(entity);
      return deleted;
    } catch (err) {
      throw new InternalServerErrorException();
    }
  }
}
