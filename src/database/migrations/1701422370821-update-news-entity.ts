import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateNewsEntity1701422370821 implements MigrationInterface {
  name = 'UpdateNewsEntity1701422370821';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`news\` CHANGE \`view\` \`view\` int UNSIGNED NOT NULL DEFAULT '0'`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`news\` CHANGE \`view\` \`view\` int UNSIGNED NOT NULL`,
    );
  }
}
