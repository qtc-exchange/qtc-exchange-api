import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateNewsEntity1701479596863 implements MigrationInterface {
  name = 'UpdateNewsEntity1701479596863';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`news\` ADD \`type\` varchar(9) NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE \`news\` DROP COLUMN \`type\``);
  }
}
