import { MigrationInterface, QueryRunner } from 'typeorm';

export class RefactorRelationHashtagNews1701420783486
  implements MigrationInterface
{
  name = 'RefactorRelationHashtagNews1701420783486';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`news\` DROP FOREIGN KEY \`FK_b4193d67846cd12e60243d98535\``,
    );
    await queryRunner.query(
      `CREATE TABLE \`hashtag-news-relation\` (\`id\` varchar(36) NOT NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deletedAt\` datetime(6) NULL, \`hashtagId\` varchar(36) NULL, \`newsId\` varchar(36) NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(`ALTER TABLE \`news\` DROP COLUMN \`hashtagId\``);
    await queryRunner.query(
      `ALTER TABLE \`hashtag-news-relation\` ADD CONSTRAINT \`FK_f133753fc91be54e825a35ad690\` FOREIGN KEY (\`hashtagId\`) REFERENCES \`hashtag\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
    await queryRunner.query(
      `ALTER TABLE \`hashtag-news-relation\` ADD CONSTRAINT \`FK_039056d87451d68e877bb93c873\` FOREIGN KEY (\`newsId\`) REFERENCES \`news\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`hashtag-news-relation\` DROP FOREIGN KEY \`FK_039056d87451d68e877bb93c873\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`hashtag-news-relation\` DROP FOREIGN KEY \`FK_f133753fc91be54e825a35ad690\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`news\` ADD \`hashtagId\` varchar(36) NULL`,
    );
    await queryRunner.query(`DROP TABLE \`hashtag-news-relation\``);
    await queryRunner.query(
      `ALTER TABLE \`news\` ADD CONSTRAINT \`FK_b4193d67846cd12e60243d98535\` FOREIGN KEY (\`hashtagId\`) REFERENCES \`hashtag\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
