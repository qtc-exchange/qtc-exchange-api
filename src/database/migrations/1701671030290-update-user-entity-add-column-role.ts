import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateUserEntityAddColumnRole1701671030290 implements MigrationInterface {
    name = 'UpdateUserEntityAddColumnRole1701671030290'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` ADD \`role\` enum ('admin', 'member') NOT NULL DEFAULT 'member'`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`role\``);
    }

}
