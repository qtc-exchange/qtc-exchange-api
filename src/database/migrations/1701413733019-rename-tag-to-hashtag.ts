import { MigrationInterface, QueryRunner } from 'typeorm';

export class RenameTagToHashtag1701413733019 implements MigrationInterface {
  name = 'RenameTagToHashtag1701413733019';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`news\` DROP FOREIGN KEY \`FK_ddc38c082bbd2b789d7194f4cab\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`news\` CHANGE \`tagId\` \`hashtagId\` varchar(36) NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`tag\` CHANGE \`keyword\` \`tag\` varchar(100) NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`news\` ADD CONSTRAINT \`FK_b4193d67846cd12e60243d98535\` FOREIGN KEY (\`hashtagId\`) REFERENCES \`tag\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`news\` DROP FOREIGN KEY \`FK_b4193d67846cd12e60243d98535\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`tag\` CHANGE \`tag\` \`keyword\` varchar(100) NOT NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`news\` CHANGE \`hashtagId\` \`tagId\` varchar(36) NULL`,
    );
    await queryRunner.query(
      `ALTER TABLE \`news\` ADD CONSTRAINT \`FK_ddc38c082bbd2b789d7194f4cab\` FOREIGN KEY (\`tagId\`) REFERENCES \`tag\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
