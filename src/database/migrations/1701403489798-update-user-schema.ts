import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateUserSchema1701403489798 implements MigrationInterface {
  name = 'UpdateUserSchema1701403489798';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`password\``);
    await queryRunner.query(
      `ALTER TABLE \`user\` ADD \`password\` varchar(64) NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE \`user\` DROP COLUMN \`password\``);
    await queryRunner.query(
      `ALTER TABLE \`user\` ADD \`password\` varchar(32) NOT NULL`,
    );
  }
}
