import { MigrationInterface, QueryRunner } from "typeorm";

export class UpdateUserEntityChangeUsernameToEmail1701746053445 implements MigrationInterface {
    name = 'UpdateUserEntityChangeUsernameToEmail1701746053445'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`username\` \`email\` varchar(32) NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE \`user\` CHANGE \`email\` \`username\` varchar(32) NOT NULL`);
    }

}
