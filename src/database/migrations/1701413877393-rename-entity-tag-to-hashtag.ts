import { MigrationInterface, QueryRunner } from 'typeorm';

export class RenameEntityTagToHashtag1701413877393
  implements MigrationInterface
{
  name = 'RenameEntityTagToHashtag1701413877393';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`news\` DROP FOREIGN KEY \`FK_b4193d67846cd12e60243d98535\``,
    );
    await queryRunner.query(
      `CREATE TABLE \`hashtag\` (\`id\` varchar(36) NOT NULL, \`createdAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6), \`updatedAt\` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) ON UPDATE CURRENT_TIMESTAMP(6), \`deletedAt\` datetime(6) NULL, \`tag\` varchar(100) NOT NULL, PRIMARY KEY (\`id\`)) ENGINE=InnoDB`,
    );
    await queryRunner.query(
      `ALTER TABLE \`news\` ADD CONSTRAINT \`FK_b4193d67846cd12e60243d98535\` FOREIGN KEY (\`hashtagId\`) REFERENCES \`hashtag\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`news\` DROP FOREIGN KEY \`FK_b4193d67846cd12e60243d98535\``,
    );
    await queryRunner.query(`DROP TABLE \`hashtag\``);
    await queryRunner.query(
      `ALTER TABLE \`news\` ADD CONSTRAINT \`FK_b4193d67846cd12e60243d98535\` FOREIGN KEY (\`hashtagId\`) REFERENCES \`tag\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
