import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateHashtagNewRelationEntity1701479779846
  implements MigrationInterface
{
  name = 'UpdateHashtagNewRelationEntity1701479779846';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`hashtag-news-relation\` DROP FOREIGN KEY \`FK_039056d87451d68e877bb93c873\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`hashtag-news-relation\` ADD CONSTRAINT \`FK_039056d87451d68e877bb93c873\` FOREIGN KEY (\`newsId\`) REFERENCES \`news\`(\`id\`) ON DELETE CASCADE ON UPDATE NO ACTION`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `ALTER TABLE \`hashtag-news-relation\` DROP FOREIGN KEY \`FK_039056d87451d68e877bb93c873\``,
    );
    await queryRunner.query(
      `ALTER TABLE \`hashtag-news-relation\` ADD CONSTRAINT \`FK_039056d87451d68e877bb93c873\` FOREIGN KEY (\`newsId\`) REFERENCES \`news\`(\`id\`) ON DELETE NO ACTION ON UPDATE NO ACTION`,
    );
  }
}
