import { MigrationInterface, QueryRunner } from 'typeorm';

export class UpdateNewsEntity1701480144318 implements MigrationInterface {
  name = 'UpdateNewsEntity1701480144318';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE \`news\` DROP COLUMN \`type\``);
    await queryRunner.query(
      `ALTER TABLE \`news\` ADD \`type\` varchar(10) NOT NULL`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`ALTER TABLE \`news\` DROP COLUMN \`type\``);
    await queryRunner.query(
      `ALTER TABLE \`news\` ADD \`type\` varchar(9) NOT NULL`,
    );
  }
}
