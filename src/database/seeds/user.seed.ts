import { Seeder, Factory } from 'typeorm-seeding';
import { Repository, DataSource } from 'typeorm';
import { UserEntity } from 'database/entities/user.entity';
import * as crypto from 'node:crypto';
import { UserRoleEnum } from 'database/enum/user.enum';

export default class SeedUser implements Seeder {
  public async run(factory: Factory, connection: DataSource): Promise<void> {
    const userRepository: Repository<UserEntity> =
      connection.getRepository(UserEntity);
    const users = await userRepository.find();
    if (users.length === 0) {
      await connection
        .createQueryBuilder()
        .insert()
        .into(UserEntity)
        .values([
          {
            email: process.env.DEFAULT_USER,
            password: crypto
              .createHmac('sha256', process.env.PASSWORD_SECRET)
              .update(process.env.DEFAULT_PASS)
              .digest('hex'),
            name: process.env.DEFAULT_USER,
            role: UserRoleEnum.ADMIN,
          },
        ])
        .execute();
    }
  }
}
