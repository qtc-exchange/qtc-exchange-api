import { Entity, ManyToOne } from 'typeorm';
import { BaseEntity } from './base.entity';
import { HashtagEntity } from './hashtag.entity';
import { NewsEntity } from './news.entity';

@Entity({ name: 'hashtag-news-relation' })
export class HashtagNewsRelationEntity extends BaseEntity {
  @ManyToOne(() => HashtagEntity, (hashtag) => hashtag.hashtagNewsRelation)
  hashtag: HashtagEntity;

  @ManyToOne(() => NewsEntity, (news) => news.hashtagNewsRelation, {
    onDelete: 'CASCADE',
  })
  news: NewsEntity;
}
