import { Entity, Column, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { HashtagNewsRelationEntity } from './hashtag-news-relation.entity';

@Entity({ name: 'hashtag' })
export class HashtagEntity extends BaseEntity {
  @Column({ name: 'tag', type: 'nvarchar', length: 100 })
  tag: string;

  @OneToMany(
    () => HashtagNewsRelationEntity,
    (hashtagNewsRelation) => hashtagNewsRelation.hashtag,
  )
  hashtagNewsRelation: HashtagNewsRelationEntity[];
}
