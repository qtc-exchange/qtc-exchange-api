import { Entity, Column, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { NewsEntity } from './news.entity';
import { UserRoleEnum } from 'database/enum/user.enum';

@Entity({ name: 'user' })
export class UserEntity extends BaseEntity {
  @Column({ name: 'email', type: 'nvarchar', length: 32 })
  email: string;

  @Column({ name: 'password', type: 'nvarchar', length: 64 })
  password: string;

  @Column({ name: 'name', type: 'nvarchar', length: 50 })
  name: string;

  @Column({
    name: 'role',
    type: 'enum',
    enum: UserRoleEnum,
    default: UserRoleEnum.MEMBER,
  })
  role: UserRoleEnum;

  @OneToMany(() => NewsEntity, (news) => news.user)
  news: NewsEntity[];
}
