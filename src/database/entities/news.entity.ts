import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';
import { BaseEntity } from './base.entity';
import { UserEntity } from './user.entity';
import { HashtagNewsRelationEntity } from './hashtag-news-relation.entity';
import { NewsTypeEnum } from 'database/enum/news.enum';

@Entity({ name: 'news' })
export class NewsEntity extends BaseEntity {
  @Column({ name: 'title', type: 'nvarchar', length: 100 })
  title: string;

  @Column({ name: 'description', type: 'nvarchar', length: 2000 })
  description: string;

  @Column({ name: 'type', type: 'nvarchar', length: 10 })
  type: NewsTypeEnum;

  @Column({ name: 'view', type: 'int', unsigned: true, default: 0 })
  view: number;

  @ManyToOne(() => UserEntity)
  user: UserEntity;

  @OneToMany(
    () => HashtagNewsRelationEntity,
    (hashtagNewsRelation) => hashtagNewsRelation.news,
  )
  hashtagNewsRelation: HashtagNewsRelationEntity[];
}
