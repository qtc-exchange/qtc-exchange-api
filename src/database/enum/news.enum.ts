export enum NewsTypeEnum {
  ARTICLES = 'Articles',
  NEWS = 'News',
  PROMOTIONS = 'Promotions',
}
