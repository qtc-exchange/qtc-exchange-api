# STATE BUILD
FROM node:18-alpine AS builder

ENV APP_HOME_BUILD=/srv/app
ENV TZ=Asia/Bangkok

WORKDIR $APP_HOME_BUILD

ADD ./package.json $APP_HOME_BUILD
RUN yarn install
ADD ./ $APP_HOME_BUILD
RUN yarn build

# STATE PROD
FROM node:18-alpine AS production
ARG APP_PORT=3400 \
    DATABASE_HOST=localhost \
    DATABASE_NAME=qtc_db \
    DATABASE_PASS=******** \
    DATABASE_PORT=1366 \
    DATABASE_USER=root \
    DEFAULT_PASS=Abc123456* \
    DEFAULT_USER=admin@mail.com \
    JWT_SECRET_KEY=secret \
    PASSWORD_SECRET=secret \
    SSL=false
ENV APP_HOME_PROD=/srv/app \
    APP_PORT=$APP_PORT \
    DATABASE_HOST=$DATABASE_HOST \
    DATABASE_NAME=$DATABASE_HOST \
    DATABASE_PASS=$DATABASE_PASS \
    DATABASE_PORT=$DATABASE_PORT \
    DATABASE_USER=$DATABASE_USER \
    DEFAULT_PASS=$DEFAULT_PASS \
    DEFAULT_USER=$DEFAULT_USER \
    JWT_SECRET_KEY=$JWT_SECRET_KEY \
    PASSWORD_SECRET=$PASSWORD_SECRET \
    SSL=false \
    TZ=Asia/Bangkok 

WORKDIR $APP_HOME_PROD

ADD package.json ./
COPY --from=builder /srv/app/yarn.lock $APP_HOME_PROD/yarn.lock

RUN yarn install --production

COPY --from=builder /srv/app/dist $APP_HOME_PROD/dist

EXPOSE $APP_PORT
CMD ["/bin/sh", "-c", "node dist/main.js"]
